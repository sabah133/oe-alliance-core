KV = "4.11.0"
SRCDATE = "20170508"
KOFILES = "brcmstb-${MACHINE_DRIVER} ci ftm4862 fts260 sp988x"

require xcore-dvb-modules.inc
SRC_URI[md5sum] = "c8dd1143441dcb46fe3806af9a101bb1"
SRC_URI[sha256sum] = "4f09704266269a607a8f20e368c1e9b1fccc52f98066e0690ba6a01be5fd4438"
