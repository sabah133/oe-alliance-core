KV = "4.11.0"
SRCDATE = "20170508"
KOFILES = "brcmstb-${MACHINE_DRIVER} ci ftm4862 fts260 sp988x"

require xcore-dvb-modules.inc
SRC_URI[md5sum] = "eb648ed1f9afe232285f7438c92103bb"
SRC_URI[sha256sum] = "664579b23328cb9825058d28126c1adf31fcd625dcb07836fb01c82f0387d0fc"
