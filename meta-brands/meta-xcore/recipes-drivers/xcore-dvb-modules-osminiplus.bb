KV = "4.11.0"
SRCDATE = "20170508"
KOFILES = "brcmstb-${MACHINE_DRIVER} ci ftm4862 fts260 sp988x"

require xcore-dvb-modules.inc
SRC_URI[md5sum] = "d3c82f5f26c8008de31612d613c8a496"
SRC_URI[sha256sum] = "0be4add4a1b7eca4fbc895d000cb5bd82a27c53283b9107c8dbc4b16674a3bc0"