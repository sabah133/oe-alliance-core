KV = "4.11.0"
SRCDATE = "20170508"
KOFILES = "brcmstb-${MACHINE_DRIVER} ci ftm4862 fts260 sp988x"

require xcore-dvb-modules.inc
SRC_URI[md5sum] = "fa92e1690e2e20c2956c47b45562ded7"
SRC_URI[sha256sum] = "594951721e6480c9843513caf77221fe5837f6ab321ca5136149c482be2594a0"
