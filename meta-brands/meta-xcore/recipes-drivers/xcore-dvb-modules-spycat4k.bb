KV = "4.11.0"
SRCDATE = "20170508"
KOFILES = "brcmstb-${MACHINE_DRIVER} ci ftm4862 fts260 sp988x"

require xcore-dvb-modules.inc
SRC_URI[md5sum] = "75b43b51e3d8e0c65d379b2b3bf1bfda"
SRC_URI[sha256sum] = "c256cf61996919e270c95aecd47931e035e21b21c8aab61bceddcb7642669785"
