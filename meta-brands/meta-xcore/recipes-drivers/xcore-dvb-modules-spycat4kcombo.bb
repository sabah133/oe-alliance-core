KV = "4.11.0"
SRCDATE = "20170508"
KOFILES = "brcmstb-${MACHINE_DRIVER} ci ftm4862 fts260 sp988x"

require xcore-dvb-modules.inc
SRC_URI[md5sum] = "f221c0fc906ea92e5b15e411fa73cbad"
SRC_URI[sha256sum] = "18bd5dc9b5847a637fffe13fdb29058a28a23edd0e193aceb4f6f45bd9a72018"
