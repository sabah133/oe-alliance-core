KV = "4.11.0"
SRCDATE = "20170508"
KOFILES = "brcmstb-${MACHINE_DRIVER} ci ftm4862 fts260 sp988x"

require xcore-dvb-modules.inc
SRC_URI[md5sum] = "a1a46c5a8944f6fc0a7ff2c35ae3febb"
SRC_URI[sha256sum] = "5ffeeab33a27fe07c6420217a4c09abc938e53bc5dece0200266c6f9708326bf"
