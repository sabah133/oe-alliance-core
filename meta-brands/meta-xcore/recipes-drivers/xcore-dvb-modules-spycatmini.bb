KV = "4.11.0"
SRCDATE = "20170508"
KOFILES = "brcmstb-${MACHINE_DRIVER} ci ftm4862 fts260 sp988x"

require xcore-dvb-modules.inc
SRC_URI[md5sum] = "530a029616b24da0286d4514b802393e"
SRC_URI[sha256sum] = "826a05d3b22760f0fe4aee3c4f53ca6c4eab4403ce8d1edef2da82b73981530c"
