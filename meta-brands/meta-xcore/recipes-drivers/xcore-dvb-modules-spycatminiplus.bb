KV = "4.11.0"
SRCDATE = "20170508"
KOFILES = "brcmstb-${MACHINE_DRIVER} ci ftm4862 fts260 sp988x"

require xcore-dvb-modules.inc
SRC_URI[md5sum] = "390ddb31672bcd289398f05c2edd06dc"
SRC_URI[sha256sum] = "131a81193be3bfc37a0c0f07bf379d26099149a179f969439b91be4a4b42a2f7"
